<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Handle a login request to the application.
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws ValidationException
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);

        if ($this->attemptLogin($request)) {
            $user = $this->guard()->user();
            $user->generateToken();

            return response()->json($user);
        }

        return $this->sendFailedLoginResponse($request);
    }

    public function loginSocial(Request $request)
    {
        $oauth_token = $request->get('oauth_token');
        $fcm_token = $request->get('fcm_token');
        try {
            $user = Socialite::driver('google')->userFromToken($oauth_token);

            // check if they're an existing user
            $existingUser = User::query()->where('email', $user->email)->first();
            if ($existingUser) {
                auth()->login($existingUser, true);
                if (isset($fcm_token)) {
                    $existingUser->fcm_token = $fcm_token;
                }
                $existingUser->save();
                return response()->json($existingUser);
            }
            // create a new user
            $newUser = new User;
            $newUser->name = $user->name;
            $newUser->email = $user->email;
            if (isset($fcm_token)) {
                $newUser->fcm_token = $fcm_token;
            }
            $newUser->generateToken();
            $newUser->save();
            auth()->login($newUser, true);
            return response()->json($newUser);

        } catch (ClientException $exception) {
            return $this->sendFailedLoginResponse($request);
        }
    }

    /**
     * Log the user out of the application.
     *
     * @param Request $request
     * @return Response
     */
    public function logout(Request $request)
    {
        $user = Auth::guard('api')->user();

        if ($user) {
            $user->api_token = null;
            $user->save();
        }

        return response()->json(['data' => 'User logged out.'], JsonResponse::HTTP_OK);
    }
}
