<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\User;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class FirebaseCloudMessagingController extends Controller
{
    public function send(Request $request)
    {
        $ids = $request->get('user_ids');
        $users = User::query()
                    ->whereIn('id', $ids)
                    ->whereNotNull('fcm_token')
                    ->get();
        $tokens = array_pluck($users, 'fcm_token');

        if(sizeof($tokens)) {
            $title = $request->get('title');
            $body = $request->get('body');
            $click_action = $request->get('click_action');
            $icon = $request->get('icon');
    
            $optionBuilder = new OptionsBuilder();
            $optionBuilder->setTimeToLive(60*20);
            $option = $optionBuilder->build();
    
            $notificationBuilder = new PayloadNotificationBuilder($title);
            $notificationBuilder->setBody($body)
                            ->setIcon($icon)
                            ->setClickAction($click_action);
            $notification = $notificationBuilder->build();
    
            $downstreamResponse = FCM::sendTo($tokens, $option, $notification);
    
            //return Array - you must remove all this tokens in your database
            $downstreamResponse->tokensToDelete();
    
            //return Array (key : oldToken, value : new token - you must change the token in your database )
            $downstreamResponse->tokensToModify();
    
            //return Array - you should try to resend the message to the tokens in the array
            $downstreamResponse->tokensToRetry();
    
            // return Array (key:token, value:errror) - in production you should remove from your database the tokens present in this array
            $downstreamResponse->tokensWithError();
    
            return response()->json([
                    'initial_users_to_notify_count' => sizeof($ids),
                    'subscribed_user_count' => sizeof($tokens),
                    'success_sent_count' => $downstreamResponse->numberSuccess(),
                    'failure_sent_count' => $downstreamResponse->numberFailure(),
                    'modification_sent_count' => $downstreamResponse->numberModification(),
                ], JsonResponse::HTTP_OK);
        }
        return response()->json([
                'message' => "no one is subscribed",
            ], 412);
    }
}
