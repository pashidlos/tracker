<?php

namespace App\Http\Controllers;

use App\Record;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class RecordController extends Controller
{
    public function history(Request $request)
    {
        $startDate = Carbon::create($request->get('startDate'))->startOfDay();
        $endDate = Carbon::create($request->get('endDate'))->endOfDay();
        $records = $request->user()->records()
            ->whereBetween('date', [$startDate, $endDate])
            ->orderBy('date')
            ->get();

        $response = [
            'startDate' => $startDate,
            'endDate' => $endDate,
            'records' => $records
        ];

        return response()->json($response, JsonResponse::HTTP_OK);
    }

    public function show(Record $record)
    {
        return $record;
    }

    public function store(Request $request)
    {
        /** @var User $user */
        $user = $request->user();
        /** @var Record $record */
        $record = $request->all();

        if ($user->records()
            ->where('date', 'LIKE', Carbon::parse($record['date'])->format('Y-m-d') . '%')
            ->where('type', $request->input('type'))
            ->exists()) {
            return response()->json(null, JsonResponse::HTTP_CONFLICT);
        }
        $record = $user->records()->create($record);

        return response()->json($record, JsonResponse::HTTP_CREATED);
    }

    public function update(Request $request, Record $record)
    {
        $record->update($request->all());

        return response()->json($record, JsonResponse::HTTP_OK);
    }

    public function delete(Record $record)
    {
        /** @var Record $record */
        $record->delete();

        return response()->json(null, JsonResponse::HTTP_NO_CONTENT);
    }

    public function stats(Request $request)
    {
        $date = $request->get('date');
        $stats = $this->getRecords($date);

        return response()->json($stats, JsonResponse::HTTP_OK);
    }

    private function getRecords($date)
    {
        $users = User::all();

        $result = array();
        foreach ($users as $user) {
            $result[$user->id] = $user->records()
                ->where('date', 'LIKE', $date . '%')
                ->get();
        }
        return $result;
    }

    public function invalid(Request $request)
    {
        $startDate = Carbon::create($request->get('date'))->startOfDay();
        $endDate = Carbon::create($request->get('date'))->endOfDay();

        $users = User::query()->whereHas('records', function ($query) use ($startDate, $endDate) {
            $query->whereBetween('date', [$startDate, $endDate]);
        }, '<', 2)
            ->with(['records' => function ($query) use ($startDate, $endDate) {
                $query->whereBetween('date', [$startDate, $endDate]);
            }])
            ->get();

        $response = array();
        foreach ($users as $user) {
            array_push($response, [
                'id' => $user->id,
                'name' => $user->name,
                'email' => $user->email,
                'recordsHistory' => [
                    'startDate' => $startDate,
                    'endDate' => $endDate,
                    'records' => $user->records
                ]
            ]);
        }

        return response()->json($response, JsonResponse::HTTP_OK);
    }
}
