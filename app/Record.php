<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Record extends Model
{
    const TYPE_IN = 'in';
    const TYPE_OUT = 'out';

    protected $fillable = [
        'date', 'type',
    ];

    /**
     * Get the user that owns the record.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
