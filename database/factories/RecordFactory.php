<?php

use App\Record;
use Faker\Generator as Faker;

$factory->define(Record::class, function (Faker $faker) {
    return [
        'date' => \Carbon\Carbon::now(),
        'type' => $faker->randomElement([Record::TYPE_IN, Record::TYPE_OUT])
    ];
});
