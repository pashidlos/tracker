<?php

use App\Record;
use App\User;
use Illuminate\Database\Seeder;

class RecordsTableSeeder extends Seeder
{
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        Record::query()->delete();

        $faker = \Faker\Factory::create();

        $users = User::all();

        //Random users
        for ($i = 0; $i < 20; $i++) {
            $user = $users->random();
            $user->records()->create([
                'date' => $faker->dateTime,
                'type' => $faker->randomElement([Record::TYPE_IN, Record::TYPE_OUT])
            ]);
        }
    }
}
