## Requirements
- PHP 7.2
- Homestead


## Set up locally

- [Install Homestead](https://laravel.com/docs/5.8/homestead)
- Edit Homestead.yml  as in example
```
---
ip: "192.168.10.10"
memory: 2048
cpus: 1
provider: virtualbox
version: 6.4.0 # Higher versions doesn't have php 7.0 installed but could be updated

authorize: ~/.ssh/id_rsa.pub

keys:
    - ~/.ssh/id_rsa

folders:
    - map: ~/local-env/         #path to folder with your projects on your machine
      to: /home/vagrant/Code    #where to put it in Vagrant machine

sites:
    - map: tracker.local
      to: /home/vagrant/Code/tracker/public
      php: "7.2" 

databases:
    - homestead
  ```
<!-- - add in `hosts`
```
192.168.10.10 tracker.local
``` -->
- Run Vagrant from Homestead directory
```
vagrant up
```
- Install all dependencies
```
composer install 
npm install
npm run dev
```
- Create `.env` file from `.env.example` it supose to have all settings
- [Create postgress DB inside Vagrant and seed prepared data](http://billmartin.io/blog/2018/04/29/Configure-Laravel-to-Use-PostgreSQL-with-Homestead/) - DB name should correspond to DB_DATABASE in`.env`
- [Create Google OAuth client id for Web application](https://console.developers.google.com/apis/credentials)
- Set `Authorized JavaScript origins` and `Authorized redirect URIs`
![alt text](/docs/OAuth_settings.png)
- Add OAuth GOOGLE_CLIENT_ID and GOOGLE_CLIENT_SECRET in `.env`
- open `http://localhost:8000/`