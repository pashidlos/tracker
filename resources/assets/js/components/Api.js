import axios from 'axios/index'

class Api {

    login(user) {
        return axios.post('/api/login', user)
    }

    login_google(oauth_token, fcm_token) {
        return axios.get('/api/login', { params: { 
            oauth_token: oauth_token,
            fcm_token: fcm_token
        }})
    }

    postRecord(record) {
        return axios.post('/api/records', record, { headers: { 'Authorization': `Bearer ${this.getToken()}` } })
    }

    getRecordsHistory(dateRange) {
        return axios.post('/api/records/history', dateRange, { headers: { 'Authorization': `Bearer ${this.getToken()}` } })
    }

    getInvalidRecords(date) {
        return axios.post('/api/records/invalid', date, { headers: { 'Authorization': `Bearer ${this.getToken()}` } })
    }

    fcmSend(date) {
        return axios.post('/api/fcm/send', date, { headers: { 'Authorization': `Bearer ${this.getToken()}` } })
    }

    getToken() {
        const user = JSON.parse(sessionStorage.getItem('user'));
        return user && user.api_token
    }
}

const api = new Api();

export default api