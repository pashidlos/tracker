import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import { initializeFirebase, askForPermissioToReceiveNotifications } from '../push-notification';


import Header from './Header'
import Home from './Home'
import Report from "./Report";
import History from "./History";
import api from "./Api";

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            user: JSON.parse(sessionStorage.getItem('user'))
        }
    }

    logout = () => {
        this.setState({
            user: null
        });
        sessionStorage.removeItem('user');
    };

    loginBackend = (response) => {
        const fcm_token = localStorage.getItem('fcm_token');
        api.login_google(response.accessToken, fcm_token).then(response => {
            console.log(response.data)
            const user = response.data;
            this.setState({
                user: user,
            });
            sessionStorage.setItem('user', JSON.stringify(user))
        })
    };

    onFailure = (error) => {
        alert(error);
    };

    render() {
        return (
            <BrowserRouter>
                <Header user={this.state.user} loginBackend={this.loginBackend} logout={this.logout} />
                <Switch>
                    <Route exact path='/' render={(props) => <Home {...props} user={this.state.user} />} />
                    <Route exact path='/history' render={(props) => <History {...props} user={this.state.user} />} />
                    <Route exact path='/report' render={(props) => <Report {...props} user={this.state.user} />} />
                </Switch>
            </BrowserRouter>
        )
    }
}

if (document.getElementById('app')) {
    ReactDOM.render(<App />, document.getElementById('app'))
    initializeFirebase();
    askForPermissioToReceiveNotifications()
}
