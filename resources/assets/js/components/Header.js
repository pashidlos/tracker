import React, {Component} from 'react'
import {Link} from 'react-router-dom'
import {GoogleLogin} from "react-google-login";

class Header extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let content = !!this.props.user ?
            (
                <div className="navbar-collapse collapse" id="collapsingNavbar">
                    <ul className="navbar-nav">
                    <li className="nav-item">
                            <Link className="nav-link" to="/history">History</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/report">Report</Link>
                        </li>
                    </ul>
                    <ul className="navbar-nav ml-auto">
                        <li className="nav-item dropdown">
                            <a className="nav-link dropdown-toggle"
                               href="#"
                               role="button"
                               data-toggle="dropdown"
                               aria-haspopup="true"
                               aria-expanded="false">
                                {this.props.user.name}
                            </a>
                            <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a className="dropdown-item"
                                   onClick={this.props.logout}>
                                    Log out
                                </a>
                            </div>
                        </li>
                    </ul>
                </div>
            ) :
            (
                <div>
                    <GoogleLogin
                        clientId="750189537977-scp6v8m19n7hab6b9mch2q13co9us8vk.apps.googleusercontent.com"
                        buttonText="Login"
                        onSuccess={this.props.loginBackend}
                        onFailure={this.props.onFailure}
                    />
                </div>
            );
        return (
            <nav className='navbar navbar-expand-lg navbar-light bg-light'>
                <Link className='navbar-brand' to='/'>Tracker</Link>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsingNavbar">
                    <span className="navbar-toggler-icon"></span>
                </button>
                {content}
            </nav>
        )
    }
}

export default Header