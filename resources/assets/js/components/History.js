import React, {Component} from "react";
import DatePicker from "react-datepicker/es";
import moment from "moment";
import RecordList from "./records/RecordsListComponent";
import api from "./Api";

class History extends Component {
    constructor(props) {
        super(props);
        this.state = {
            startDate: moment(new Date()).subtract(7, 'days').toDate(),
            endDate: new Date(),
            recordsHistory: {
                records: []
            }
        };
    }

    getRecordsHistory = () => {
        api.getRecordsHistory({
            startDate: this.state.startDate,
            endDate: this.state.endDate,
        }).then(response => {
            this.setState({
                recordsHistory: response.data
            })
        });
    };

    componentDidMount = () => {
        this.getRecordsHistory()
    };

    handleChangeStartDate = (date) => {
        this.setState({
            startDate: date
        });
    };

    handleChangeEndDate = (date) => {
        this.setState({
            endDate: date
        });
    };

    handleClick = () => {
        this.getRecordsHistory()
    };

    render() {
        return (
            <div className='container py-4'>
                <DatePicker
                    name='date'
                    autoComplete='off'
                    dateFormat="yyyy-MM-dd"
                    selected={this.state.startDate}
                    onChange={this.handleChangeStartDate}
                />
                <DatePicker
                    name='date'
                    autoComplete='off'
                    dateFormat="yyyy-MM-dd"
                    selected={this.state.endDate}
                    onChange={this.handleChangeEndDate}
                />
                <button className='btn btn-primary'
                        onClick={this.handleClick}>
                    Get history
                </button>
                <RecordList recordsHistory={this.state.recordsHistory}/>
            </div>
        )
    }

}

export default History