import React, { Component } from 'react'
import DatePicker from "react-datepicker";
import queryString from 'query-string';

import "react-datepicker/dist/react-datepicker.css";
import RecordList from './records/RecordsListComponent';
import api from './Api'

class Home extends Component {
    constructor(props) {
        super(props);

        const date = new Date(queryString.parse(this.props.location.search).date);
        this.state = {
            date: date instanceof Date && !isNaN(date) ? date : new Date(),
            recordsHistory: {
                records: []
            }
        };
    }

    getRecordsHistory = (date) => {
        api.getRecordsHistory({
            startDate: date,
            endDate: date,
        }).then(response => {
            this.setState({
                recordsHistory: response.data
            })
        });
    };

    componentDidMount = () => {
        this.getRecordsHistory(this.state.date)
    };

    handleDateChange = (date) => {
        this.setState({
            date: date
        });
    };

    handleDateSelect = (date) => {
        this.setState({
            date: date
        }, this.getRecordsHistory(date));
    };

    handleClick = (type) => {
        const record = {
            date: this.state.date.toISOString(),
            type: type
        };

        api.postRecord(record).then(response => {
            this.setState(prevState => {
                const newState = prevState.recordsHistory
                newState.records = [...prevState.recordsHistory.records, response.data]
                return {
                    recordsHistory: newState
                }
            })
        });
    };

    render() {
        return (
            <div className='container py-4'>
                <DatePicker
                    name='date'
                    autoComplete='off'
                    showTimeSelect
                    dateFormat="yyyy-MM-dd HH:mm"
                    timeFormat="HH:mm"
                    selected={this.state.date}
                    onChange={this.handleDateChange}
                    onSelect={this.handleDateSelect}
                />
                <button className='btn btn-primary' onClick={() => this.handleClick('in')}>
                    Check-in
                </button>
                <button className='btn btn-primary' onClick={() => this.handleClick('out')}>
                    Check-out
                </button>
                <RecordList recordsHistory={this.state.recordsHistory} />
            </div>
        )
    }
}

export default Home