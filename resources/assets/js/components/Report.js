import React, { Component } from 'react'

import api from './Api'
import DatePicker from "react-datepicker";
import moment from "moment";

import "react-datepicker/dist/react-datepicker.css";
import UserList from "./users/UserListComponent";

class Report extends Component {
    constructor(props) {
        super(props);
        this.state = {
            date: new Date(),
            users: []
        };
    }

    handleChange = (date) => {
        this.setState({
            date: date
        })
    };

    fsmSend = () => {
        const request = {
            user_ids: this.state.users.map(user => user.id),
            title: "Please, fill missing data",
            body: "Please check records for " + moment(this.state.date).format('dddd, MMMM Do'),
            click_action: "https://ciklum-tracker.herokuapp.com?date=" + moment(this.state.date).format('YYYY-MM-DD'),
            icon: "https://vignette2.wikia.nocookie.net/villains/images/9/9f/Bender_Rodr%C3%ADguez.jpg/revision/latest?cb=20150929231612"
        };

        api.fcmSend(request)
            .then(response => {
                alert("Sent " + response.data.success_sent_count + " messages!")
            }).catch(error => {
                alert(error.response.data.message);
            })
    };

    getInvalidRecords = () => {
        const request = {
            date: this.state.date
        };

        api.getInvalidRecords(request)
            .then(response => {
                this.setState({
                    users: response.data
                });
            })
    };

    componentDidMount = () => {
        this.getInvalidRecords()
    };

    render() {
        return (
            <div className='container py-4'>
                <div className='row justify-content-center'>
                    <DatePicker
                        autoComplete='off'
                        dateFormat="yyyy-MM-dd"
                        selected={this.state.date}
                        onChange={this.handleChange}
                    />
                    <button className='btn btn-primary' onClick={this.getInvalidRecords}>Get invalid records</button>
                    <button className='btn btn-primary' onClick={this.fsmSend}>Ping users</button>
                </div>
                <UserList users={this.state.users} />
            </div>
        );
    }
}

export default Report;