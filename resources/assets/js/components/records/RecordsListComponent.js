import React, { Component } from 'react'
import moment from "moment";

var formatDate = function (date) {
    return moment(date).format('YYYY-MM-DD dddd');
}

var addDays = function(date, daysOffset) {
    var date = new Date(date.valueOf());
    date.setDate(date.getDate() + daysOffset);
    return date;
}

// Returns an array of dates between the two dates
var getDatesArray = function (startDate, endDate) {
    let dates = [];
    let currentDate = new Date(startDate);

    while (currentDate <= new Date(endDate)) {
        dates[formatDate(currentDate)] = [];
        currentDate = addDays(currentDate, 1);
    }
    return dates;
};

class RecordList extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        const { recordsHistory } = this.props

        const datesArray = getDatesArray(recordsHistory.startDate, recordsHistory.endDate)

        recordsHistory.records.forEach(function (value) {
            const date = formatDate(value.date);
            const time = moment(value.date).format('HH:mm');
            const type = value.type;
            datesArray[date][type] = time
        });

        return (
            <table className="table">
                <thead>
                    <tr>
                        <th scope="col">Date</th>
                        <th scope="col">In</th>
                        <th scope="col">Out</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        Object.keys(datesArray).map((key) =>
                            <tr key={key}>
                                <td>
                                    {key}
                                </td>
                                <td>
                                    {datesArray[key]['in'] ? datesArray[key]['in'] : "No"}
                                </td>
                                <td>
                                    {datesArray[key]['out'] ? datesArray[key]['out'] : "No"}
                                </td>
                            </tr>
                        )
                    }
                </tbody>
            </table>
        );
    }
}

export default RecordList