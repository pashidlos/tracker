import React from 'react'
import RecordList from "../records/RecordsListComponent";


function UserList(props) {
    const users = props.users;

    return (
        <table className="table">
            <thead>
            <tr>
                <th scope="col">Name</th>
                <th scope="col">Email</th>
                <th scope="col">Records</th>
            </tr>
            </thead>
            <tbody>
            {
                users.map((user) =>
                    <tr key={user.id}>
                        <td>
                            {user.name}
                        </td>
                        <td>
                            {user.email}
                        </td>
                        <td>
                            <RecordList recordsHistory={user.recordsHistory}/>
                        </td>
                    </tr>
                )
            }
            </tbody>
        </table>
    );
}

export default UserList