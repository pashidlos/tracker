<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/register', 'Auth\RegisterController@register');
Route::post('/login', 'Auth\LoginController@login');
Route::get('/login', 'Auth\LoginController@loginSocial');
Route::post('/logout', 'Auth\LoginController@logout');

Route::group(['middleware' => 'auth:api'], function() {
    Route::post('/records/history', 'RecordController@history');
    Route::post('/records', 'RecordController@store');
    Route::get('/records/{record}', 'RecordController@show');
    Route::put('/records/{record}', 'RecordController@update');
    Route::delete('/records/{record}', 'RecordController@delete');
    Route::post('/records/stats', 'RecordController@stats');
    Route::post('/records/invalid', 'RecordController@invalid');

    Route::get('/user', 'UserController@index');

    Route::post('/fcm/send', 'FirebaseCloudMessagingController@send');
});