<?php

namespace Tests\Feature;

use App\Record;
use App\User;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RecordTest extends TestCase
{
    public function testCanCreateRecord()
    {
        $user = factory(User::class)->create();
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];
        $payload = [
            'date' => '2019-06-11 13:26:53',
            'type' => Record::TYPE_IN,
        ];

        $this->json('POST', '/api/records', $payload, $headers)
            ->assertStatus(201)
            ->assertJson([
                'user_id' => $user->id,
                'date' => '2019-06-11 13:26:53',
                'type' => Record::TYPE_IN,
            ]);
    }

    public function testCanUpdateRecord()
    {
        $user = factory(User::class)->create();
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];
        $record = factory(Record::class)->create([
            'user_id' => $user->id,
            'date' => '2019-06-11 13:26:53',
            'type' => Record::TYPE_IN,
        ]);

        $payload = [
            'date' => '2019-06-12 15:22:51',
            'type' => Record::TYPE_OUT,
        ];

        $response = $this->json('PUT', '/api/records/' . $record->id, $payload, $headers)
            ->assertStatus(200)
            ->assertJson([
                'date' => '2019-06-12 15:22:51',
                'type' => Record::TYPE_OUT,
            ]);
    }

    public function testsCanDeleteRecord()
    {
        $user = factory(User::class)->create();
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];
        $record = factory(Record::class)->create([
            'user_id' => $user->id,
            'date' => '2019-06-11 13:26:53',
            'type' => Record::TYPE_IN,
        ]);

        $this->json('DELETE', '/api/records/' . $record->id, [], $headers)
            ->assertStatus(204);
    }

    public function testRecordsAreListedCorrectly()
    {
        $user = factory(User::class)->create();
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];

        factory(Record::class)->create([
            'user_id' => $user->id,
            'date' => '2019-06-11 13:26:53',
            'type' => Record::TYPE_IN,
        ]);

        factory(Record::class)->create([
            'user_id' => $user->id,
            'date' => '2019-06-12 13:26:51',
            'type' => Record::TYPE_OUT,
        ]);


        $response = $this->json('GET', '/api/records', [], $headers)
            ->assertStatus(200)
            ->assertJson([
                [
                    'user_id' => $user->id,
                    'date' => '2019-06-11 13:26:53',
                    'type' => Record::TYPE_IN,
                ],
                [
                    'user_id' => $user->id,
                    'date' => '2019-06-12 13:26:51',
                    'type' => Record::TYPE_OUT,
                ],
            ])
            ->assertJsonStructure([
                '*' => ['id', 'date', 'user_id', 'type', 'created_at', 'updated_at'],
            ]);
    }
}
